//
//  GPUImageAdaptiveThresholdFilter+Additive.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageAdaptiveThresholdFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageAdaptiveThresholdFilter (Additive)

@property (strong, nonatomic, readonly) IAImageFilterSettings *blurRadiusInPixelsSettings;

@end
