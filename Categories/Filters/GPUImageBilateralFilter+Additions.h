//
//  GPUImageBilateralFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageBilateralFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageBilateralFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *distanceNormalizationFactorSettings;

@end
