//
//  GPUImageBilateralFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageBilateralFilter+Additions.h"

@implementation GPUImageBilateralFilter (Additions)

-(IAImageFilterSettings*)distanceNormalizationFactorSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 100.0;
    return filterSettings;
}

@end
