//
//  GPUImageBrightnessFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageBrightnessFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageBrightnessFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *brightnessSettings;

@end
