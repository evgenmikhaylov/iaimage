//
//  GPUImageBrightnessFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageBrightnessFilter+Additions.h"

@implementation GPUImageBrightnessFilter (Additions)

-(IAImageFilterSettings*)brightnessSettings;
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = -1.0;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

@end
