//
//  GPUImageContrastFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageContrastFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageContrastFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *contrastSettings;

@end
