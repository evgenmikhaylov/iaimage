//
//  GPUImageContrastFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageContrastFilter+Additions.h"

@implementation GPUImageContrastFilter (Additions)

-(IAImageFilterSettings*)contrastSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 4.0;
    return filterSettings;
}

@end
