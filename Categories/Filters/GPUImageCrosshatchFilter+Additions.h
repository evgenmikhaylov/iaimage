//
//  GPUImageCrosshatchFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageCrosshatchFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageCrosshatchFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *crossHatchSpacingSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *lineWidthSettings;

@end
