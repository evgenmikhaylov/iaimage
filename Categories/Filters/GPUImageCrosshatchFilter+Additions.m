//
//  GPUImageCrosshatchFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageCrosshatchFilter+Additions.h"

@implementation GPUImageCrosshatchFilter (Additions)

-(IAImageFilterSettings*)crossHatchSpacingSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.03;
    filterSettings.maximumValue = 0.06;
    return filterSettings;
}

-(IAImageFilterSettings*)lineWidthSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.003;
    filterSettings.maximumValue = 0.006;
    return filterSettings;
}

@end
