//
//  GPUImageEmbossFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageEmbossFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageEmbossFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *intensitySettings;

@end
