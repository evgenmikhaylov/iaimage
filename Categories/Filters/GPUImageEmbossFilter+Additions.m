//
//  GPUImageEmbossFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageEmbossFilter+Additions.h"

@implementation GPUImageEmbossFilter (Additions)

-(IAImageFilterSettings*)intensitySettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.4;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

@end
