//
//  GPUImageExposureFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageExposureFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageExposureFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *exposureSettings;

@end
