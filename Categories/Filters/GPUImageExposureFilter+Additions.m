//
//  GPUImageExposureFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageExposureFilter+Additions.h"

@implementation GPUImageExposureFilter (Additions)

-(IAImageFilterSettings*)exposureSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = -3.0;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 3.0;
    return filterSettings;
}

@end
