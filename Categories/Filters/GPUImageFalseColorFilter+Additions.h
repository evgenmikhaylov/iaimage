//
//  GPUImageFalseColorFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageFalseColorFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageFalseColorFilter (Additions)

@property (assign, nonatomic) CGFloat firstColorRed;
@property (assign, nonatomic) CGFloat firstColorGreen;
@property (assign, nonatomic) CGFloat firstColorBlue;
@property (assign, nonatomic) CGFloat secondColorRed;
@property (assign, nonatomic) CGFloat secondColorGreen;
@property (assign, nonatomic) CGFloat secondColorBlue;

@property (strong, nonatomic, readonly) IAImageFilterSettings *firstColorRedSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *firstColorGreenSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *firstColorBlueSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *secondColorRedSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *secondColorGreenSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *secondColorBlueSettings;

@end
