//
//  GPUImageFalseColorFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageFalseColorFilter+Additions.h"

@implementation GPUImageFalseColorFilter (Additions)

-(IAImageFilterSettings*)firstColorRedSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    filterSettings.displayedMinimumValue = 0.0;
    filterSettings.displayedMaximumValue = 255.0;
    filterSettings.stepValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)firstColorGreenSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    filterSettings.displayedMinimumValue = 0.0;
    filterSettings.displayedMaximumValue = 255.0;
    filterSettings.stepValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)firstColorBlueSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    filterSettings.displayedMinimumValue = 0.0;
    filterSettings.displayedMaximumValue = 255.0;
    filterSettings.stepValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)secondColorRedSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    filterSettings.displayedMinimumValue = 0.0;
    filterSettings.displayedMaximumValue = 255.0;
    filterSettings.stepValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)secondColorGreenSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    filterSettings.displayedMinimumValue = 0.0;
    filterSettings.displayedMaximumValue = 255.0;
    filterSettings.stepValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)secondColorBlueSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    filterSettings.displayedMinimumValue = 0.0;
    filterSettings.displayedMaximumValue = 255.0;
    filterSettings.stepValue = 1.0;
    return filterSettings;
}

-(void)setFirstColorRed:(CGFloat)firstColorRed
{
    self.firstColor = (GPUVector4){firstColorRed, self.firstColor.two, self.firstColor.three, 1.0f};
}

-(CGFloat)firstColorRed
{
    return self.firstColor.one;
}

-(void)setFirstColorGreen:(CGFloat)firstColorGreen
{
    self.firstColor = (GPUVector4){self.firstColor.one, firstColorGreen, self.firstColor.three, 1.0f};
}

-(CGFloat)firstColorGreen
{
    return self.firstColor.two;
}

-(void)setFirstColorBlue:(CGFloat)firstColorBlue
{
    self.firstColor = (GPUVector4){self.firstColor.one, self.firstColor.two, firstColorBlue, 1.0f};
}

-(CGFloat)firstColorBlue
{
    return self.firstColor.three;
}

-(void)setSecondColorRed:(CGFloat)secondColorRed
{
    self.secondColor = (GPUVector4){secondColorRed, self.secondColor.two, self.secondColor.three, 1.0f};
}

-(CGFloat)secondColorRed
{
    return self.secondColor.one;
}

-(void)setSecondColorGreen:(CGFloat)secondColorGreen
{
    self.secondColor = (GPUVector4){self.secondColor.one, secondColorGreen, self.secondColor.three, 1.0f};
}

-(CGFloat)secondColorGreen
{
    return self.firstColor.two;
}

-(void)setSecondColorBlue:(CGFloat)secondColorBlue
{
    self.secondColor = (GPUVector4){self.secondColor.one, self.secondColor.two, secondColorBlue, 1.0f};
}

-(CGFloat)secondColorBlue
{
    return self.secondColor.three;
}

@end
