//
//  GPUImageGammaFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageGammaFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageGammaFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *gammaSettings;

@end
