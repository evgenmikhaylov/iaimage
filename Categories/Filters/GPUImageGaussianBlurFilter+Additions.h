//
//  GPUImageGaussianBlurFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageGaussianBlurFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageGaussianBlurFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *texelSpacingMultiplierSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *blurRadiusInPixelsSettings;

@end
