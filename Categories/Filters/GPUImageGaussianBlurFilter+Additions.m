//
//  GPUImageGaussianBlurFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageGaussianBlurFilter+Additions.h"

@implementation GPUImageGaussianBlurFilter (Additions)

-(IAImageFilterSettings*)texelSpacingMultiplierSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 1.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

-(IAImageFilterSettings*)blurRadiusInPixelsSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 1.0;
    filterSettings.defaultValue = 2.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

@end
