//
//  GPUImageGaussianBlurPositionFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageGaussianBlurPositionFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageGaussianBlurPositionFilter (Additions)

@property (assign, nonatomic) CGFloat blurCenterX;
@property (assign, nonatomic) CGFloat blurCenterY;

@property (strong, nonatomic, readonly) IAImageFilterSettings *blurSizeSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *blurCenterXSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *blurCenterYSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *blurRadiusSettings;

@end
