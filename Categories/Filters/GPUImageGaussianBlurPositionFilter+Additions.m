//
//  GPUImageGaussianBlurPositionFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageGaussianBlurPositionFilter+Additions.h"

@implementation GPUImageGaussianBlurPositionFilter (Additions)

-(IAImageFilterSettings*)blurSizeSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

-(IAImageFilterSettings*)blurCenterXSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)blurCenterYSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(void)setBlurCenterX:(CGFloat)blurCenterX
{
    self.blurCenter = CGPointMake(blurCenterX, self.blurCenter.y);
}

-(CGFloat)blurCenterX
{
    return self.blurCenter.x;
}

-(void)setBlurCenterY:(CGFloat)blurCenterY
{
    self.blurCenter = CGPointMake(self.blurCenter.x, blurCenterY);
}

-(CGFloat)blurCenterY
{
    return self.blurCenter.y;
}

-(IAImageFilterSettings*)blurRadiusSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

@end
