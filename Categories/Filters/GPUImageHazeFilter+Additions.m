//
//  GPUImageHazeFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageHazeFilter+Additions.h"

@implementation GPUImageHazeFilter (Additions)

-(IAImageFilterSettings*)distanceSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = -0.3;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 0.3;
    return filterSettings;
}

-(IAImageFilterSettings*)slopeSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = -0.3;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 0.3;
    return filterSettings;
}

@end
