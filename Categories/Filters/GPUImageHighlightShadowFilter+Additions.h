//
//  GPUImageHighlightShadowFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageHighlightShadowFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageHighlightShadowFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *shadowsSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *highlightsSettings;

@end
