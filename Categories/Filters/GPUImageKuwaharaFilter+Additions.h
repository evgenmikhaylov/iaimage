//
//  GPUImageKuwaharaFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageKuwaharaFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageKuwaharaFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *radiusSettings;

@end
