//
//  GPUImageKuwaharaFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageKuwaharaFilter+Additions.h"

@implementation GPUImageKuwaharaFilter (Additions)

-(IAImageFilterSettings*)radiusSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 3.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

@end
