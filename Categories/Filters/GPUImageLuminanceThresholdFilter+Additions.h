//
//  GPUImageLuminanceThresholdFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageLuminanceThresholdFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageLuminanceThresholdFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *thresholdSettings;

@end
