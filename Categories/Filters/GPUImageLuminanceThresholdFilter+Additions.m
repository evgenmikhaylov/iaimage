//
//  GPUImageLuminanceThresholdFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageLuminanceThresholdFilter+Additions.h"

@implementation GPUImageLuminanceThresholdFilter (Additions)

-(IAImageFilterSettings*)thresholdSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

@end
