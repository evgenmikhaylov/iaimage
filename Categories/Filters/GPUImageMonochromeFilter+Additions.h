//
//  GPUImageMonochromeFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageMonochromeFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageMonochromeFilter (Additions)

@property (assign, nonatomic) CGFloat red;
@property (assign, nonatomic) CGFloat green;
@property (assign, nonatomic) CGFloat blue;

@property (strong, nonatomic, readonly) IAImageFilterSettings *intensitySettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *redSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *greenSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *blueSettings;

@end
