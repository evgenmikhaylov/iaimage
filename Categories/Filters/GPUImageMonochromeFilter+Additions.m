//
//  GPUImageMonochromeFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageMonochromeFilter+Additions.h"

@implementation GPUImageMonochromeFilter (Additions)

-(IAImageFilterSettings*)intensitySettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)redSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)greenSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)blueSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(void)setRed:(CGFloat)red
{
    self.color = (GPUVector4){red, self.color.two, self.color.three, 1.0f};
}

-(CGFloat)red
{
    return self.color.one;
}

-(void)setGreen:(CGFloat)green
{
    self.color = (GPUVector4){self.color.one, green, self.color.three, 1.0f};
}

-(CGFloat)green
{
    return self.color.two;
}

-(void)setBlue:(CGFloat)blue
{
    self.color = (GPUVector4){self.color.one, self.color.two, blue, 1.0f};
}

-(CGFloat)blue
{
    return self.color.three;
}


@end
