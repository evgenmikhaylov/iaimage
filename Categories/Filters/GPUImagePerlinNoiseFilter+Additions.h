//
//  GPUImagePerlinNoiseFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImagePerlinNoiseFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImagePerlinNoiseFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *scaleSettings;

@end
