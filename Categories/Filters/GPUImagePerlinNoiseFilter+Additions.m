//
//  GPUImagePerlinNoiseFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImagePerlinNoiseFilter+Additions.h"

@implementation GPUImagePerlinNoiseFilter (Additions)

-(IAImageFilterSettings*)scaleSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 8.0;
    filterSettings.maximumValue = 100.0;
    return filterSettings;
}

@end
