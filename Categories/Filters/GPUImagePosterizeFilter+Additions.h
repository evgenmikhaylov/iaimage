//
//  GPUImagePosterizeFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImagePosterizeFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImagePosterizeFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *colorLevelsSettings;

@end
