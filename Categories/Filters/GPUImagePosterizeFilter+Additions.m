//
//  GPUImagePosterizeFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImagePosterizeFilter+Additions.h"

@implementation GPUImagePosterizeFilter (Additions)

-(IAImageFilterSettings*)colorLevelsSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 10.0;
    filterSettings.maximumValue = 100.0;
    return filterSettings;
}

@end
