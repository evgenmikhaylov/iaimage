//
//  GPUImageRGBFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageRGBFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageRGBFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *redSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *greenSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *blueSettings;

@end
