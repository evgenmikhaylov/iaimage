//
//  GPUImageRGBFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageRGBFilter+Additions.h"

@implementation GPUImageRGBFilter (Additions)

-(IAImageFilterSettings*)redSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)greenSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)blueSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

@end
