//
//  GPUImageBrightnessFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSaturationFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageSaturationFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *saturationSettings;

@end
