//
//  GPUImageBrightnessFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSaturationFilter+Additions.h"

@implementation GPUImageSaturationFilter (Additions)

-(IAImageFilterSettings*)saturationSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 2.0;
    return filterSettings;
}

@end
