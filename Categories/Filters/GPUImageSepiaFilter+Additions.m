//
//  GPUImageSepiaFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSepiaFilter+Additions.h"

@implementation GPUImageSepiaFilter (Additions)

-(IAImageFilterSettings*)intensitySettings;
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 1.0;
    filterSettings.stepValue = 0.01;
    return filterSettings;
}

@end
