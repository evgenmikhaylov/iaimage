//
//  GPUImageSharpenFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSharpenFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageSharpenFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *sharpnessSettings;

@end
