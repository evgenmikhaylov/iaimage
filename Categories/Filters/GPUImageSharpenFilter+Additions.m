//
//  GPUImageSharpenFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSharpenFilter+Additions.h"

@implementation GPUImageSharpenFilter (Additions)

-(IAImageFilterSettings*)sharpnessSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = -4.0;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 4.0;
    return filterSettings;
}

@end
