//
//  GPUImageSketchFilter+Additive.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSketchFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageSketchFilter (Additive)

@property (strong, nonatomic, readonly) IAImageFilterSettings *edgeStrengthSettings;

@end
