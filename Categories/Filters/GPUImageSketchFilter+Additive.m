//
//  GPUImageSketchFilter+Additive.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSketchFilter+Additive.h"

@implementation GPUImageSketchFilter (Additive)

-(IAImageFilterSettings*)edgeStrengthSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.25;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

@end
