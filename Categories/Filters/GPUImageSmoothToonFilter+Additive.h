//
//  GPUImageSmoothToonFilter+Additive.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSmoothToonFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageSmoothToonFilter (Additive)

@property (strong, nonatomic, readonly) IAImageFilterSettings *blurRadiusInPixelsSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *thresholdSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *quantizationLevelsSettings;

@end
