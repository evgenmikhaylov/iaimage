//
//  GPUImageSmoothToonFilter+Additive.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSmoothToonFilter+Additive.h"

@implementation GPUImageSmoothToonFilter (Additive)

-(IAImageFilterSettings*)blurRadiusInPixelsSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 2.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

-(IAImageFilterSettings*)thresholdSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.1;
    filterSettings.defaultValue = 0.2;
    filterSettings.maximumValue = 2.0;
    return filterSettings;
}

-(IAImageFilterSettings*)quantizationLevelsSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 1.0;
    filterSettings.defaultValue = 10.0;
    filterSettings.maximumValue = 20.0;
    return filterSettings;
}

@end
