//
//  GPUImageStretchDistortionFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageStretchDistortionFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageStretchDistortionFilter (Additions)

@property (assign, nonatomic) CGFloat centerX;
@property (assign, nonatomic) CGFloat centerY;

@property (strong, nonatomic, readonly) IAImageFilterSettings *centerXSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *centerYSettings;

@end
