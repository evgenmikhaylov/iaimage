//
//  GPUImageStretchDistortionFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageStretchDistortionFilter+Additions.h"

@implementation GPUImageStretchDistortionFilter (Additions)

-(IAImageFilterSettings*)centerXSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)centerYSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(void)setCenterX:(CGFloat)centerX
{
    self.center = CGPointMake(centerX, self.center.y);
}

-(CGFloat)centerX
{
    return self.center.x;
}

-(void)setCenterY:(CGFloat)centerY
{
    self.center = CGPointMake(self.center.x, centerY);
}

-(CGFloat)centerY
{
    return self.center.y;
}

@end
