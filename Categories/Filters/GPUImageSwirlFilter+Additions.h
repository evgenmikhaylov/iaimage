//
//  GPUImageSwirlFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageSwirlFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageSwirlFilter (Additions)

@property (assign, nonatomic) CGFloat centerX;
@property (assign, nonatomic) CGFloat centerY;

@property (strong, nonatomic, readonly) IAImageFilterSettings *centerXSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *centerYSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *radiusSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *angleSettings;

@end
