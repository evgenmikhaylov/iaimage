//
//  GPUImageTiltShiftFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageTiltShiftFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageTiltShiftFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *blurRadiusInPixelsSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *topFocusLevelSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *bottomFocusLevelSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *focusFallOffRateSettings;

@end
