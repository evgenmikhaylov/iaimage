//
//  GPUImageTiltShiftFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageTiltShiftFilter+Additions.h"

@implementation GPUImageTiltShiftFilter (Additions)

-(IAImageFilterSettings*)blurRadiusInPixelsSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 7.0;
    filterSettings.maximumValue = 20.0;
    return filterSettings;
}

-(IAImageFilterSettings*)topFocusLevelSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.4;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)bottomFocusLevelSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.6;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)focusFallOffRateSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.2;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

@end
