//
//  GPUImageToonFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageToonFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageToonFilter (Additions)

@property (strong, nonatomic, readonly) IAImageFilterSettings *thresholdSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *quantizationLevelsSettings;

@end
