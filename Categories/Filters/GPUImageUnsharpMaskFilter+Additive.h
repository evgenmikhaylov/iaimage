//
//  GPUImageUnsharpMaskFilter+Additive.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageUnsharpMaskFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageUnsharpMaskFilter (Additive)

@property (strong, nonatomic, readonly) IAImageFilterSettings *blurRadiusInPixelsSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *intensitySettings;

@end
