//
//  GPUImageUnsharpMaskFilter+Additive.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageUnsharpMaskFilter+Additive.h"

@implementation GPUImageUnsharpMaskFilter (Additive)

-(IAImageFilterSettings*)blurRadiusInPixelsSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 1.0;
    filterSettings.defaultValue = 4.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

-(IAImageFilterSettings*)intensitySettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 1.0;
    filterSettings.maximumValue = 10.0;
    return filterSettings;
}

@end
