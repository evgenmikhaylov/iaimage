//
//  GPUImageVignetteFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageVignetteFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageVignetteFilter (Additions)

@property (assign, nonatomic) CGFloat vignetteCenterX;
@property (assign, nonatomic) CGFloat vignetteCenterY;

@property (strong, nonatomic, readonly) IAImageFilterSettings *vignetteCenterXSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *vignetteCenterYSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *vignetteStartSettings;
@property (strong, nonatomic, readonly) IAImageFilterSettings *vignetteEndSettings;

@end
