//
//  GPUImageVignetteFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageVignetteFilter+Additions.h"

@implementation GPUImageVignetteFilter (Additions)

-(IAImageFilterSettings*)vignetteCenterXSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)vignetteCenterYSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(void)setVignetteCenterX:(CGFloat)vignetteCenterX
{
    self.vignetteCenter = CGPointMake(vignetteCenterX, self.vignetteCenter.y);
}

-(CGFloat)vignetteCenterX
{
    return self.vignetteCenter.x;
}

-(void)setVignetteCenterY:(CGFloat)vignetteCenterY
{
    self.vignetteCenter = CGPointMake(self.vignetteCenter.x, vignetteCenterY);
}

-(CGFloat)vignetteCenterY
{
    return self.vignetteCenter.y;
}

-(IAImageFilterSettings*)vignetteStartSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.5;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

-(IAImageFilterSettings*)vignetteEndSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.75;
    filterSettings.maximumValue = 1.0;
    return filterSettings;
}

@end
