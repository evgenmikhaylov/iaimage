//
//  GPUImageVoronoiConsumerFilter+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageVoronoiConsumerFilter.h"
#import "IAImageFilterSettings.h"

@interface GPUImageVoronoiConsumerFilter (Additions)

@property (assign, nonatomic) CGFloat widthInPixels;
@property (strong, nonatomic, readonly) IAImageFilterSettings *widthInPixelsSettings;

@end
