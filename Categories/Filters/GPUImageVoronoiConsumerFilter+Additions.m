//
//  GPUImageVoronoiConsumerFilter+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/2/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageVoronoiConsumerFilter+Additions.h"

@implementation GPUImageVoronoiConsumerFilter (Additions)

-(IAImageFilterSettings*)widthInPixelsSettings
{
    IAImageFilterSettings *filterSettings = [[IAImageFilterSettings alloc] init];
    filterSettings.minimumValue = 0.0;
    filterSettings.defaultValue = 0.0;
    filterSettings.maximumValue = 2048.0;
    return filterSettings;
}

-(void)setWidthInPixels:(CGFloat)widthInPixels
{
    self.sizeInPixels = CGSizeMake(widthInPixels, widthInPixels);
}

-(CGFloat)widthInPixels
{
    return self.sizeInPixels.width;
}

@end
