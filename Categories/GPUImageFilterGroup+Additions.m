//
//  GPUImageFilterGroup+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 6/16/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageFilterGroup+Additions.h"

@implementation GPUImageFilterGroup (Additions)

-(NSArray *)targets
{
    return self.terminalFilter.targets;
}

@end
