//
//  GPUImageOutput+Additions.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/29/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageOutput.h"
#import "IAImageFilterSettings.h"

@interface GPUImageOutput (Additions)

-(NSString*)filterTitle;
+(NSString*)filterTitleWithClass:(Class)filterClass;
+(NSString*)filterTitleWithClassName:(NSString*)filterClassName;
-(IAImageFilterSettings*)settingsForProperty:(NSString*)propertyName;
-(NSArray*)settingsList;

@end
