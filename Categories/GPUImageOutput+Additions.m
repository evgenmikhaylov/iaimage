//
//  GPUImageOutput+Additions.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/29/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "GPUImageOutput+Additions.h"
#import <objc/runtime.h>

@implementation GPUImageOutput (Additions)

#pragma mark - FilterTitle

-(NSString*)filterTitle
{
    return [GPUImageOutput filterTitleWithClass:self.class];
}

+(NSString*)filterTitleWithClass:(Class)filterClass
{
    return [GPUImageOutput filterTitleWithClassName:NSStringFromClass(filterClass)];
}

+(NSString*)filterTitleWithClassName:(NSString*)filterClassName
{
    return [[filterClassName stringByReplacingOccurrencesOfString:@"GPUImage" withString:@""] stringByReplacingOccurrencesOfString:@"Filter" withString:@""];
}

#pragma mark - Settings

-(IAImageFilterSettings*)settingsForProperty:(NSString*)propertyName
{
    NSString *settingsMethodName = [NSString stringWithFormat:@"%@Settings",propertyName];
    IAImageFilterSettings *filterSettings = [self valueForKeyPath:settingsMethodName];
    return filterSettings;
}

-(NSArray*)settingsList
{
    NSMutableArray *list = [@[] mutableCopy];
    Class class = self.class;
    while (class!=[GPUImageOutput class])
    {
        unsigned int count=0;
        objc_property_t *props = class_copyPropertyList(class,&count);
        for (int i=0; i<count; i++)
        {
            const char *name = property_getName(props[i]);
            NSString *propertyName = [NSString stringWithUTF8String:name];
            if ([propertyName rangeOfString:@"Settings"].location!=NSNotFound){
                [list addObject:[propertyName stringByReplacingOccurrencesOfString:@"Settings" withString:@""]];
            }
        }
        class = [class superclass];
    }
    return list;
}


@end
