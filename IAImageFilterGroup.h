//
//  IAFilterGroup.h
//  IAImage
//
//  Created by EvgenyMikhaylov on 4/22/14.
//  Copyright (c) 2014 Evgeny Mikhaylov. All rights reserved.
//

#import "GPUImageFilterGroup.h"
#import <GPUImage.h>

@interface GPUImageOutput (TargetSelection)

-(NSArray*)targetsWithClass:(Class)targetClass;
-(NSArray*)targetsWithoutClasses:(NSArray*)targetClasses;

@end

@interface IAImageFilterGroup : GPUImageFilterGroup

@property (strong, nonatomic) NSMutableArray *sourcePictures;
@property (strong, nonatomic) NSString *title;

-(void)addBlendFilter:(GPUImageOutput<GPUImageInput> *)newFilter image:(UIImage*)image;
-(void)addBlendFilter:(GPUImageOutput<GPUImageInput> *)newFilter color:(UIColor*)color;
-(void)insertFilter:(GPUImageOutput<GPUImageInput>*)newFilter atIndex:(NSUInteger)index;
-(void)insertBlendFilter:(GPUImageOutput<GPUImageInput>*)newFilter atIndex:(NSUInteger)index image:(UIImage*)image;
-(void)insertBlendFilter:(GPUImageOutput<GPUImageInput>*)newFilter atIndex:(NSUInteger)index color:(UIColor*)color;
-(void)removeFilterAtIndex:(NSUInteger)index;
-(void)removeFiltersAtIndexes:(NSIndexSet*)indexes;
-(void)removeFilter:(GPUImageOutput<GPUImageInput>*)filter;
-(void)moveFilterFromIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;
-(void)enableFilter:(BOOL)enable atIndex:(NSUInteger)index;
-(NSInteger)enabledFiltersCount;
-(GPUImageOutput<GPUImageInput>*)previousFilter:(NSUInteger)index;
-(GPUImageOutput<GPUImageInput>*)nextFilter:(NSUInteger)index;
-(void)processAllImages;
-(void)processAllImagesWithCompletionHandler:(void (^)(void))completion;
-(NSArray*)filtersArray;

@end
