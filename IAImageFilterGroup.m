//
//  IAFilterGroup.m
//  IAImage
//
//  Created by EvgenyMikhaylov on 4/22/14.
//  Copyright (c) 2014 Evgeny Mikhaylov. All rights reserved.
//

#import "IAImageFilterGroup.h"
#import "IAImagePicture.h"
#import "IAImageLayer.h"

@implementation GPUImageOutput (TargetSelection)

-(NSArray*)targetsWithClass:(Class)targetClass
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class == %@", targetClass];
    NSArray *resultTargets = [self.targets filteredArrayUsingPredicate:predicate];
    return resultTargets;
}

-(NSArray*)targetsWithoutClasses:(NSArray*)targetClasses
{
    NSMutableArray *predicates = [@[] mutableCopy];
    for (Class targetClass in targetClasses) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"class != %@", targetClass]];
    }
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    NSArray *resultTargets = [self.targets filteredArrayUsingPredicate:compoundPredicate];
    return resultTargets;
}

@end

@implementation UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end

@interface IAImageFilterGroup ()

@end

@implementation IAImageFilterGroup

- (id)init
{
    if (!(self = [super init]))
    {
		return nil;
    }
    
    self.sourcePictures = [@[] mutableCopy];
    
    return self;
}

#pragma mark - Filters

-(void)addFilter:(GPUImageOutput<GPUImageInput>*)newFilter
{
    GPUImageOutput<GPUImageInput> *previousFilter = [self previousFilter:self.filterCount];

    [super addFilter:newFilter];
    
    if (previousFilter)
    {
        GPUImageView *gpuImageView = [[self.terminalFilter targetsWithClass:[GPUImageView class]] firstObject];
        if (gpuImageView){
            [newFilter addTarget:gpuImageView];
        }
        [self.terminalFilter removeAllTargets];
        [self.terminalFilter addTarget:newFilter];
    }
    else{
        [self setInitialFilters:@[newFilter]];
    }

    [self setTerminalFilter:newFilter];
}

-(void)addBlendFilter:(GPUImageOutput<GPUImageInput>*)newFilter image:(UIImage*)image
{
    NSUInteger filterCount = [self filterCount];

    IAImagePicture *sourcePicture = [[IAImagePicture alloc] initWithImage:image];
    sourcePicture.index = filterCount;
    [sourcePicture addTarget:newFilter atTextureLocation:1];
    [sourcePicture processImage];
    [self.sourcePictures addObject:sourcePicture];
    
    [self addFilter:newFilter];
}

-(void)addBlendFilter:(GPUImageOutput<GPUImageInput>*)newFilter color:(UIColor*)color
{
    UIImage *colorImage = [UIImage imageWithColor:color];
    [self addBlendFilter:newFilter image:colorImage];
}

-(void)insertFilter:(GPUImageOutput<GPUImageInput>*)newFilter atIndex:(NSUInteger)index
{
    [filters insertObject:newFilter atIndex:index];
    
    NSUInteger filterCount = [self enabledFiltersCount];
    
    GPUImageOutput<GPUImageInput> *previousFilter = [self previousFilter:index];
    GPUImageOutput<GPUImageInput> *nextFilter = [self nextFilter:index];

    //targets
    if (filterCount>1)
    {
        if (!previousFilter){
            [newFilter addTarget:nextFilter];
        }
        if (previousFilter&&nextFilter)
        {
            [previousFilter removeTarget:nextFilter];
            [previousFilter addTarget:newFilter];
            [newFilter addTarget:nextFilter];
        }
        if (!nextFilter)
        {
            GPUImageView *gpuImageView = [[self.terminalFilter targetsWithClass:[GPUImageView class]] firstObject];
            if (gpuImageView){
                [newFilter addTarget:gpuImageView];
            }
            [previousFilter removeAllTargets];
            [previousFilter addTarget:newFilter];
        }
    }
    
    //initial filters
    if (!previousFilter){
        [self setInitialFilters:@[newFilter]];
    }
    
    //terminal filter
    if (!nextFilter){
        [self setTerminalFilter:newFilter];
    }
}

-(void)insertBlendFilter:(GPUImageOutput<GPUImageInput>*)newFilter atIndex:(NSUInteger)index image:(UIImage*)image
{
    IAImagePicture *sourcePicture = [[IAImagePicture alloc] initWithImage:image];
    sourcePicture.index = index;
    [sourcePicture addTarget:newFilter atTextureLocation:1];
    [sourcePicture processImage];
    [self.sourcePictures addObject:sourcePicture];
    
    [self insertFilter:newFilter atIndex:index];
}

-(void)insertBlendFilter:(GPUImageOutput<GPUImageInput>*)newFilter atIndex:(NSUInteger)index color:(UIColor*)color
{
    UIImage *colorImage = [UIImage imageWithColor:color];
    [self insertBlendFilter:newFilter atIndex:index image:colorImage];
}

-(void)removeFilterAtIndex:(NSUInteger)index
{
    NSUInteger filterCount = [self enabledFiltersCount];
    
    GPUImageOutput<GPUImageInput> *filter = [self filterAtIndex:index];
    GPUImageOutput<GPUImageInput> *previousFilter = [self previousFilter:index];
    GPUImageOutput<GPUImageInput> *nextFilter = [self nextFilter:index];

    //blend filter picture
    if ([filter isKindOfClass:[GPUImageTwoInputFilter class]]){
        [self removePictureAtIndex:index];
    }
    
    //targets
    if (filterCount>1)
    {
        if (!previousFilter){
            [filter removeTarget:nextFilter];
        }
        if (previousFilter&&nextFilter)
        {
            [previousFilter removeTarget:filter];
            [filter removeTarget:nextFilter];
            [previousFilter addTarget:nextFilter];
        }
        if (!nextFilter)
        {
            GPUImageView *gpuImageView = [[self.terminalFilter targetsWithClass:[GPUImageView class]] firstObject];
            if (gpuImageView)
            {
                [previousFilter addTarget:gpuImageView];
                [filter removeTarget:gpuImageView];
            }
            [previousFilter removeTarget:filter];
        }
    }

    //initial filters
    if (!previousFilter){
        if (filterCount>1){
            [self setInitialFilters:@[nextFilter]];
        }else{
            [self setInitialFilters:nil];
        }
    }
    
    //terminal filter
    if (!nextFilter){
        if (filterCount>1){
            [self setTerminalFilter:previousFilter];
        }else{
            [self setTerminalFilter:nil];
        }
    }
    
    [filters removeObject:filter];
}

-(void)removeFiltersAtIndexes:(NSIndexSet*)indexes
{
    __block NSUInteger count = 0;
    __weak typeof (self) weakSelf = self;
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [weakSelf removeFilterAtIndex:idx-count];
        count++;
    }];
}

-(void)removeFilter:(GPUImageOutput<GPUImageInput>*)filter
{
    NSUInteger index = [filters indexOfObject:filter];
    [self removeFilterAtIndex:index];
}

-(void)moveFilterFromIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex
{
    if (fromIndex==toIndex)
        return;
    
    GPUImageOutput<GPUImageInput> *filter = [self filterAtIndex:fromIndex];
    //blend filter picture
    if ([filter isKindOfClass:[GPUImageTwoInputFilter class]])
    {
        IAImagePicture *sourcePicture = [self pictureAtIndex:fromIndex];
        sourcePicture.index = toIndex;
    }
    if (filter.enabled)
    {
        [self removeFilterAtIndex:fromIndex];
        [self insertFilter:filter atIndex:toIndex];
    }
    else
    {
        [filters removeObjectAtIndex:fromIndex];
        [filters insertObject:filter atIndex:toIndex];
    }
}

-(void)enableFilter:(BOOL)enable atIndex:(NSUInteger)index
{
    GPUImageOutput<GPUImageInput> *filter = [self filterAtIndex:index];
    if (filter.enabled==enable)
        return;
    
    
    if (enable)
    {
        filter.enabled = enable;
        [filters removeObjectAtIndex:index];
        [self insertFilter:filter atIndex:index];
    }
    else
    {
        [self removeFilterAtIndex:index];
        [filters insertObject:filter atIndex:index];
        filter.enabled = enable;
    }
}

-(NSInteger)enabledFiltersCount
{
    NSArray *enabledFilters = [filters filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"enabled == YES"]];
    return enabledFilters.count;
}

-(GPUImageOutput<GPUImageInput>*)previousFilter:(NSUInteger)index
{
    if (index==0)
        return nil;
    NSRange range = NSMakeRange(0, index);
    NSArray *previousFilters = [filters subarrayWithRange:range];
    previousFilters = [previousFilters filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"enabled == YES"]];
    
    if (previousFilters.count==0)
        return nil;
    
    return previousFilters.lastObject;
}

-(GPUImageOutput<GPUImageInput>*)nextFilter:(NSUInteger)index
{
    if (index>=(filters.count-1))
        return nil;
    
    NSRange range = NSMakeRange(index+1, filters.count-(index+1));
    NSArray *nextFilters = [filters subarrayWithRange:range];
    nextFilters = [nextFilters filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"enabled == YES"]];
    
    if (nextFilters.count==0)
        return nil;
    
    return nextFilters.firstObject;
}

#pragma mark - Pictures

-(void)removePictureAtIndex:(NSUInteger)index
{
    GPUImagePicture *sourcePicture = [self pictureAtIndex:index];
    [self.sourcePictures removeObject:sourcePicture];
}

-(IAImagePicture*)pictureAtIndex:(NSUInteger)index
{
    NSArray *pictures = [self.sourcePictures filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"index=%d",index]];
    if (pictures.count==0)
        return nil;
    
    return pictures[0];
}

-(void)processAllImages
{
    [self.sourcePictures enumerateObjectsUsingBlock:^(IAImagePicture *sourcePicture, NSUInteger idx1, BOOL *stop) {
        [sourcePicture processImage];
    }];
    [self.filtersArray enumerateObjectsUsingBlock:^(IAImageLayer *layer, NSUInteger idx2, BOOL *stop) {
        if ([layer isKindOfClass:[IAImageLayer class]]&&layer.enabled){
                [layer processAllImages];
        }
    }];
}

-(void)processAllImagesWithCompletionHandler:(void (^)(void))completion
{
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);

    __weak typeof (self) weakSelf = self;
    void(^layersBlock)()=^{
        
        if (self.filtersArray.count==0)
        {
            if (completion){
                completion();
            }
        }
        else
        {
            [weakSelf.filtersArray enumerateObjectsUsingBlock:^(IAImageLayer *layer, NSUInteger idx2, BOOL *stop) {
                
                if ([layer isKindOfClass:[IAImageLayer class]])
                {
                    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                    [layer processAllImagesWithCompletionHandler:^{
                        
                        dispatch_semaphore_signal(semaphore);
                        if (idx2==weakSelf.filtersArray.count-1){
                            if (completion){
                                completion();
                            }
                        }
                    }];
                }
                else{
                    if (idx2==weakSelf.filtersArray.count-1){
                        if (completion){
                            completion();
                        }
                    }
                }
            }];
        }
    };
    
    if (self.sourcePictures.count==0)
    {
        layersBlock();
    }
    else
    {
        [self.sourcePictures enumerateObjectsUsingBlock:^(IAImagePicture *sourcePicture, NSUInteger idx1, BOOL *stop) {
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            [sourcePicture processImageWithCompletionHandler:^{
                dispatch_semaphore_signal(semaphore);
                if (idx1==(self.sourcePictures.count-1)){
                    layersBlock();
                }
            }];
        }];
    }
}

#pragma mark - Targets

-(void)removeAllTargets
{
    for (GPUImageOutput <GPUImageInput> *filter in self.filtersArray){
        [filter removeAllTargets];
    }
    for (IAImagePicture *sourcePicture in self.sourcePictures){
        [sourcePicture removeAllTargets];
    }
}

#pragma mark - Getters

-(NSArray*)filtersArray
{
    return filters;
}


@end
