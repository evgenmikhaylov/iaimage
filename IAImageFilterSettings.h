//
//  IAImageFilterSettings.h
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IAImageFilterSettings : NSObject

@property (assign, nonatomic) CGFloat minimumValue;
@property (assign, nonatomic) CGFloat defaultValue;
@property (assign, nonatomic) CGFloat maximumValue;
@property (assign, nonatomic) CGFloat displayedMinimumValue;
@property (assign, nonatomic) CGFloat displayedMaximumValue;
@property (assign, nonatomic) CGFloat stepValue;

-(CGFloat)currentValue:(CGFloat)value;
-(CGFloat)currentDisplayedValue:(CGFloat)value;

@end
