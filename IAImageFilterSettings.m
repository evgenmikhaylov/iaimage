//
//  IAImageFilterSettings.m
//  ImageFiltersEditor
//
//  Created by EvgenyMikhaylov on 5/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "IAImageFilterSettings.h"

@interface IAImageFilterSettings ()

@property (assign, nonatomic) BOOL displayedMinimumValueChanged;
@property (assign, nonatomic) BOOL displayedMaximumValueChanged;
@property (assign, nonatomic) BOOL stepValueChanged;

@end

@implementation IAImageFilterSettings

@synthesize displayedMinimumValue = _displayedMinimumValue;
@synthesize displayedMaximumValue = _displayedMaximumValue;
@synthesize stepValue = _stepValue;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.minimumValue = 0.0;
        self.maximumValue = 1.0;
        self.defaultValue = 0.0;
        _displayedMinimumValue = self.minimumValue;
        _displayedMaximumValue = self.maximumValue;
        _stepValue = (self.displayedMaximumValue-self.displayedMinimumValue)/100;
        self.displayedMinimumValueChanged = NO;
        self.displayedMaximumValueChanged = NO;
        self.stepValueChanged = NO;
    }
    return self;
}

-(CGFloat)displayedMinimumValue
{
    return self.displayedMinimumValueChanged ? _displayedMinimumValue : _minimumValue;
}

-(CGFloat)displayedMaximumValue
{
    return self.displayedMaximumValueChanged ? _displayedMaximumValue : _maximumValue;
}

-(void)setDisplayedMinimumValue:(CGFloat)displayedMinimumValue
{
    self.displayedMinimumValueChanged = YES;
    _displayedMinimumValue = displayedMinimumValue;
}

-(void)setDisplayedMaximumValue:(CGFloat)displayedMaximumValue
{
    self.displayedMaximumValueChanged = YES;
    _displayedMaximumValue = displayedMaximumValue;
}

-(CGFloat)currentValue:(CGFloat)value
{
    return self.maximumValue*value/self.displayedMaximumValue;
}

-(CGFloat)currentDisplayedValue:(CGFloat)value
{
    return self.displayedMaximumValue*value/self.maximumValue;
}

-(CGFloat)stepValue
{
    return self.stepValueChanged ? _stepValue : (self.displayedMaximumValue-self.displayedMinimumValue)/100;
}

-(void)setStepValue:(CGFloat)stepValue
{
    self.stepValueChanged = YES;
    _stepValue = stepValue;

}

@end
