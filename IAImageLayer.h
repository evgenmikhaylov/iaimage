//
//  IALayer.h
//  IAImage
//
//  Created by EvgenyMikhaylov on 4/28/14.
//  Copyright (c) 2014 Evgeny Mikhaylov. All rights reserved.
//

#import "IAImageFilterGroup.h"

@class IAImagePicture;

@interface IAImageLayer : IAImageFilterGroup

@property (strong, nonatomic) UIImage *image;
@property (assign, nonatomic) CGFloat opacity;
@property (assign, nonatomic) Class blendClass;
@property (assign, nonatomic) NSUInteger index;
@property (assign, nonatomic) BOOL firstLayer;

-(id)initFirstLayer:(BOOL)firstLayer;
-(id)initFirstLayer:(BOOL)firstLayer withImage:(UIImage*)image;
-(GPUImageTwoInputFilter *)blendFilter;
-(GPUImageAlphaBlendFilter *)alphaBlendFilter;
-(void)setPreviousLayerTarget:(GPUImageOutput<GPUImageInput> *)previousLayerTarget;

+(NSArray*)layerBlendingModes;
+(NSArray*)layerFiltersList;
+(NSArray*)layerSimpleFiltersList;

@end
