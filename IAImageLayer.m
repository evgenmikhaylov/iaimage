//
//  IALayer.m
//  IAImage
//
//  Created by EvgenyMikhaylov on 4/28/14.
//  Copyright (c) 2014 Evgeny Mikhaylov. All rights reserved.
//

#import "IAImageLayer.h"
#import "IAImagePicture.h"
#import <GPUImage.h>

@interface IAImageLayer ()

@property (strong, nonatomic) GPUImageTwoInputFilter *blendFilter;
@property (strong, nonatomic) GPUImageAlphaBlendFilter *alphaBlendFilter;
@property (strong, nonatomic) IAImagePicture *sourcePicture;
@property (weak, nonatomic) GPUImageOutput <GPUImageInput> *previousLayerTarget;

@end

@implementation IAImageLayer

- (id)initFirstLayer:(BOOL)firstLayer
{
    if (!(self = [super init])){
		return nil;
    }
    
    self.firstLayer = firstLayer;
    self.opacity = 1.0;

    if (self.firstLayer)
    {
        self.blendFilter = nil;
        self.alphaBlendFilter = nil;
    }
    else
    {
        _blendClass = [GPUImageNormalBlendFilter class];
        self.blendFilter = [[_blendClass alloc] init];
        self.alphaBlendFilter = [[GPUImageAlphaBlendFilter alloc] init];
        [self.alphaBlendFilter setMix:0.0];
        [self.blendFilter addTarget:self.alphaBlendFilter atTextureLocation:0];
    }

    return self;
}

- (id)init
{
    if (!(self = [self initFirstLayer:NO])){
		return nil;
    }
    return self;
}

- (id)initFirstLayer:(BOOL)firstLayer withImage:(UIImage*)image
{
    if (!(self = [self initFirstLayer:firstLayer])){
		return nil;
    }
    
    [self setImage:image];
    
    return self;
}

#pragma mark - Setters

-(void)setImage:(UIImage *)image
{
    _image = image;

    GPUImageView *gpuImageView = nil;
    GPUImageOutput <GPUImageInput> *blendFilter = nil;
    GPUImageAlphaBlendFilter *alphaBlendFilter = nil;
    if (self.sourcePicture)
    {
        if (!self.alphaBlendFilter)
        {
            gpuImageView = [[[self layerTarget] targetsWithClass:[GPUImageView class]] firstObject];
            blendFilter = [[[self layerTarget] targetsWithoutClasses:@[[GPUImageView class], [GPUImageAlphaBlendFilter class]]] firstObject];
            alphaBlendFilter = [[[self layerTarget] targetsWithClass:[GPUImageAlphaBlendFilter class]] firstObject];
        }
        [self.sourcePicture removeAllTargets];
        self.sourcePicture = nil;
    }
    
    self.sourcePicture = [[IAImagePicture alloc] initWithImage:_image];
    if ([self enabledFiltersCount]>0){
        [self.sourcePicture addTarget:self atTextureLocation:1];
    }
    if (self.alphaBlendFilter){
        [[self layerTarget] addTarget:self.blendFilter atTextureLocation:1];
    }
    else
    {
        if (blendFilter){
            [[self layerTarget] addTarget:blendFilter atTextureLocation:0];
        }
        if (alphaBlendFilter){
            [[self layerTarget] addTarget:alphaBlendFilter atTextureLocation:1];
        }
        if (gpuImageView){
            [[self layerTarget] addTarget:gpuImageView];
        }
    }
}

-(void)setBlendClass:(Class)blendClass
{
    if (self.firstLayer)
        return;

    _blendClass = blendClass;

    [self.previousLayerTarget removeTarget:self.blendFilter];
    [self.previousLayerTarget removeTarget:self.alphaBlendFilter];
    [self.blendFilter removeTarget:self.alphaBlendFilter];
    [[self layerTarget] removeTarget:self.blendFilter];
    
    self.blendFilter = nil;
    self.blendFilter = [[_blendClass alloc] init];
    [self.blendFilter addTarget:self.alphaBlendFilter atTextureLocation:0];
    [[self layerTarget] addTarget:self.blendFilter atTextureLocation:1];
    
    [self.previousLayerTarget addTarget:self.blendFilter atTextureLocation:0];
    [self.previousLayerTarget addTarget:self.alphaBlendFilter atTextureLocation:1];
}

-(void)setOpacity:(CGFloat)opacity
{
    _opacity = opacity;
    [self.alphaBlendFilter setMix:1.0-opacity];
}

#pragma mark - Targets

-(void)addTarget:(id<GPUImageInput>)newTarget atTextureLocation:(NSInteger)textureLocation
{
    if ([newTarget isKindOfClass:[IAImageLayer class]])
    {
        id target = self.alphaBlendFilter ?: [self layerTarget];
        [(IAImageLayer*)newTarget setPreviousLayerTarget:target];
        [target addTarget:[(IAImageLayer*)newTarget blendFilter] atTextureLocation:0];
        [target addTarget:[(IAImageLayer*)newTarget alphaBlendFilter] atTextureLocation:1];
    }
    else if ([newTarget isKindOfClass:[GPUImageView class]])
    {
        id target = self.alphaBlendFilter ?: [self layerTarget];
        [target addTarget:newTarget];
    }
    else{
        [super addTarget:newTarget atTextureLocation:textureLocation];
    }
}

-(void)removeTarget:(id<GPUImageInput>)targetToRemove
{
    if ([targetToRemove isKindOfClass:[IAImageLayer class]])
    {
        id target = self.alphaBlendFilter ?: [self layerTarget];
        [(IAImageLayer*)targetToRemove setPreviousLayerTarget:nil];
        [target removeTarget:[(IAImageLayer*)targetToRemove blendFilter]];
        [target removeTarget:[(IAImageLayer*)targetToRemove alphaBlendFilter]];
    }
    else if ([targetToRemove isKindOfClass:[GPUImageView class]])
    {
        id target = self.alphaBlendFilter ?: [self layerTarget];
        [target removeTarget:targetToRemove];
    }
    else{
        [super removeTarget:targetToRemove];
    }
}

-(void)removeAllTargets
{    
    id target = self.alphaBlendFilter ?: [self layerTarget];
    [target removeAllTargets];
}

-(NSArray*)targets
{
    id target = self.alphaBlendFilter ?: [self layerTarget];
    return [target targets];
}

-(id)layerTarget
{
    return ([self enabledFiltersCount]>0) ? self.terminalFilter : self.sourcePicture;
}

#pragma mark - Source Picture

-(void)processAllImages
{
    [super processAllImages];
    [self.sourcePicture processImage];
}

-(void)processAllImagesWithCompletionHandler:(void (^)(void))completion
{
    __weak typeof (self) weakSelf = self;
    [super processAllImagesWithCompletionHandler:^{
        [weakSelf.sourcePicture processImageWithCompletionHandler:completion];
    }];
}

#pragma mark - Filters

-(void)addFilter:(GPUImageOutput<GPUImageInput>*)newFilter
{
    GPUImageView *gpuImageView = [[[self layerTarget] targetsWithClass:[GPUImageView class]] firstObject];
    GPUImageOutput<GPUImageInput> *blendFilter = [[[self layerTarget] targetsWithoutClasses:@[[GPUImageView class], [GPUImageAlphaBlendFilter class]]] firstObject];
    
    [super addFilter:newFilter];
    
    if ([self enabledFiltersCount]==1)
    {
        [self.sourcePicture removeAllTargets];
        [self.sourcePicture addTarget:self atTextureLocation:1];
        if (gpuImageView){
            [newFilter addTarget:gpuImageView];
        }
    }
    if (blendFilter){
        [newFilter addTarget:blendFilter atTextureLocation:1];
    }
}

-(void)insertFilter:(GPUImageOutput<GPUImageInput>*)newFilter atIndex:(NSUInteger)index
{
    GPUImageView *gpuImageView = [[[self layerTarget] targetsWithClass:[GPUImageView class]] firstObject];
    GPUImageOutput<GPUImageInput> *blendFilter = [[[self layerTarget] targetsWithoutClasses:@[[GPUImageView class], [GPUImageAlphaBlendFilter class]]] firstObject];
    
    [super insertFilter:newFilter atIndex:index];
    
    GPUImageOutput<GPUImageInput> *nextFilter = [self nextFilter:index];
    if ([self enabledFiltersCount]==1)
    {
        [self.sourcePicture removeAllTargets];
        [self.sourcePicture addTarget:self atTextureLocation:1];
        if (gpuImageView){
            [newFilter addTarget:gpuImageView];
        }
    }
    if (!nextFilter){
        if (blendFilter){
            [newFilter addTarget:blendFilter atTextureLocation:1];
        }
    }
}

-(void)removeFilterAtIndex:(NSUInteger)index
{
    GPUImageOutput<GPUImageInput> *nextFilter = [self nextFilter:index];

    GPUImageView *gpuImageView = [[[self layerTarget] targetsWithClass:[GPUImageView class]] firstObject];
    GPUImageOutput <GPUImageInput> *blendFilter = [[[self layerTarget] targetsWithoutClasses:@[[GPUImageView class], [GPUImageAlphaBlendFilter class]]] firstObject];

    if (!nextFilter){
        [[self layerTarget] removeTarget:blendFilter];
    }

    [super removeFilterAtIndex:index];

    if ([self enabledFiltersCount]==0)
    {
        [self.sourcePicture removeAllTargets];
        if (gpuImageView){
            [self.sourcePicture addTarget:gpuImageView];
        }
    }
    if (!nextFilter){
        if (blendFilter){
            [[self layerTarget] addTarget:blendFilter atTextureLocation:1];
        }
    }    
}


#pragma mark - Class Methods

+(NSArray*)layerBlendingModes
{
    return @[
             [GPUImageNormalBlendFilter class],
             [GPUImageScreenBlendFilter class],
             [GPUImageOverlayBlendFilter class],
             [GPUImageMultiplyBlendFilter class],
             [GPUImageSubtractBlendFilter class],
             [GPUImageAddBlendFilter class],
             [GPUImageSoftLightBlendFilter class],
             [GPUImageHardLightBlendFilter class],
             [GPUImageExclusionBlendFilter class],
             [GPUImageColorDodgeBlendFilter class],
             [GPUImageColorBurnBlendFilter class],
             [GPUImageDifferenceBlendFilter class],
             [GPUImageLightenBlendFilter class],
             [GPUImageDarkenBlendFilter class],
             [GPUImageDissolveBlendFilter class],
             [GPUImageColorBlendFilter class],
             [GPUImageHueBlendFilter class],
             [GPUImageSaturationBlendFilter class],
             [GPUImageLuminosityBlendFilter class],
             [GPUImageChromaKeyBlendFilter class],
             [GPUImageDivideBlendFilter class],
             [GPUImageLinearBurnBlendFilter class],
             ];
}

+(NSArray*)layerFiltersList
{
    return @[
             [GPUImageSepiaFilter class],
             [GPUImageColorInvertFilter class],
             [GPUImageSaturationFilter class],
             [GPUImageContrastFilter class],
             [GPUImageExposureFilter class],
             [GPUImageBrightnessFilter class],
             [GPUImageLevelsFilter class],
             [GPUImageToneCurveFilter class],
             [GPUImageSharpenFilter class],
             [GPUImageGammaFilter class],
             [GPUImageHazeFilter class],
             [GPUImageLuminanceThresholdFilter class],
             [GPUImagePosterizeFilter class],
             [GPUImageCrosshatchFilter class],
             [GPUImageSwirlFilter class],
             [GPUImageVignetteFilter class],
             [GPUImageKuwaharaFilter class],
             [GPUImageGaussianBlurFilter class],
             [GPUImageGaussianBlurPositionFilter class],
             [GPUImageGaussianSelectiveBlurFilter class],
             [GPUImageGrayscaleFilter class],
             [GPUImageToonFilter class],
             [GPUImageSmoothToonFilter class],
             [GPUImageSketchFilter class],
             [GPUImageBoxBlurFilter class],
             [GPUImageAdaptiveThresholdFilter class],
             [GPUImageUnsharpMaskFilter class],
             [GPUImageStretchDistortionFilter class],
             [GPUImagePerlinNoiseFilter class],
             [GPUImageVoronoiConsumerFilter class],
             [GPUImageTiltShiftFilter class],
             [GPUImageEmbossFilter class],
             [GPUImageNonMaximumSuppressionFilter class],
             [GPUImageRGBFilter class],
             [GPUImageMedianFilter class],
             [GPUImageBilateralFilter class],
             [GPUImageErosionFilter class],
             [GPUImageRGBErosionFilter class],
             [GPUImageDilationFilter class],
             [GPUImageRGBDilationFilter class],
             [GPUImageOpeningFilter class],
             [GPUImageRGBOpeningFilter class],
             [GPUImageClosingFilter class],
             [GPUImageRGBClosingFilter class],
             [GPUImageColorPackingFilter class],
             [GPUImageMonochromeFilter class],
             [GPUImageHighlightShadowFilter class],
             [GPUImageFalseColorFilter class],
             [GPUImageAmatorkaFilter class],
             [GPUImageMissEtikateFilter class],
             [GPUImageSoftEleganceFilter class],
             [GPUImageLocalBinaryPatternFilter class],
             [GPUImageLanczosResamplingFilter class],
             [GPUImageLuminosity class],
             [GPUImageLaplacianFilter class],
             [GPUImageThresholdSketchFilter class],
             [GPUImageMosaicFilter class],
//             [GPUImagePixellateFilter class],
//             [GPUImageHalftoneFilter class],
//             [GPUImageHueFilter class],
//             [GPUImageSphereRefractionFilter class],
//             [GPUImageGlassSphereFilter class],
//             [GPUImagePolkaDotFilter class],
//             [GPUImageAverageLuminanceThresholdFilter class],
//             [GPUImageWhiteBalanceFilter class],
//             [GPUImageLowPassFilter class],
//             [GPUImageHighPassFilter class],
//             [GPUImageThresholdedNonMaximumSuppressionFilter class],
//             [GPUImageGaussianBlurPositionFilter class],
//             [GPUImagePixellatePositionFilter class],
//             [GPUImageMotionBlurFilter class],
//             [GPUImageZoomBlurFilter class],
//             [GPUImageiOSBlurFilter class],
//             [GPUImageLuminanceRangeFilter class],
             ];
}

+(NSArray*)layerSimpleFiltersList
{
    NSMutableArray *filtersList = [[IAImageLayer layerFiltersList] mutableCopy];
    NSMutableArray *classesToRemove = [@[
                                         [GPUImageToneCurveFilter class],
                                         [GPUImageLevelsFilter class],
                                         
                                         [GPUImageCrosshatchFilter class],
                                         [GPUImageGaussianBlurPositionFilter class],
                                         [GPUImageGaussianSelectiveBlurFilter class],
                                         [GPUImageVoronoiConsumerFilter class],
                                         [GPUImageTiltShiftFilter class],
                                         [GPUImageBilateralFilter class],
                                         [GPUImageAmatorkaFilter class],
                                         [GPUImageMissEtikateFilter class],
                                         [GPUImageSoftEleganceFilter class],
                                         [GPUImageLanczosResamplingFilter class],
                                         [GPUImageHSBFilter class],
                                         [GPUImageMosaicFilter class],
                                         ] mutableCopy];
    [filtersList removeObjectsInArray:classesToRemove];
    return filtersList;
}

@end
