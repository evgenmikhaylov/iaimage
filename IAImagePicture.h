//
//  IAImagePicture.h
//  IAImage
//
//  Created by EvgenyMikhaylov on 4/24/14.
//  Copyright (c) 2014 Evgeny Mikhaylov. All rights reserved.
//

#import "GPUImagePicture.h"

@interface IAImagePicture : GPUImagePicture

@property (assign, nonatomic) int index;

@end
